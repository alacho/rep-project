import React, {useState} from "react";

export function DisplayNumber(props) {
  const [number, setNumber] = useState(0);
  const addToNumber = () => {
    setNumber(prevState => {
      if (prevState <= 10) {
        return number + 1
      } else {
        return "Too high"
      }
    })
  }

  return (
    <p onClick={addToNumber}>
      {props.value}
    </p>
  )
}
