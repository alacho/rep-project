import React, {useState} from 'react';
import ReactDOM from 'react-dom'
import {MainText} from "./MainText.jsx";
import { DisplayNumber } from "./DisplayNumber.jsx";
import {HowTo} from "./HowTo.jsx";

function App() {

  return (
    <>
      <MainText />
      <HowTo />
      <DisplayNumber value={4}/>
    </>
  );
}

// State
// props

ReactDOM.render(<App />, document.getElementById('root'))
