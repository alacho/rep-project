import React, { useState, useEffect } from "react";

export function MainText() {
  const [textToDisplay, setTextToDisplay] = useState("Hallo");

  useEffect(() => {
    setTimeout(() => {
      setTextToDisplay("McDonalds")
    }, 3000);
  })

  const changeDisplayTextToSjokolade = () => {
    setTextToDisplay("Sjokoladekake")
  }

  return (
    <p onClick={changeDisplayTextToSjokolade}>
      {textToDisplay}
    </p>
  );
}
