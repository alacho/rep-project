import React, {useEffect, useState} from 'react';

const FETCH_URL = "https://jsonplaceholder.typicode.com/todos";

export function HowTo() {
  const [todos, setTodos] = useState();

  useEffect(async () => {
    const response = await fetch(FETCH_URL)
    const nextTodos = await response.json();
    setTodos(nextTodos);
  }, []);

  return (
    <>
      {todos?.map(todo => {
        const { title, completed, id } = todo;
        return (
          <ul key={id}>
            <li>{title}</li>
            <li>{completed ? "Yes" : "No"}</li>
          </ul>
        )
      })}
    </>
  )
}
